import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DatabaseDetailsComponent } from './database-details/database-details.component';
import { MainScreenComponent } from './main-screen/main-screen.component';
import { TableDetailsComponent } from './table-details/table-details.component';

const routes: Routes = [
  {
    path: '',
    component: MainScreenComponent,
    children: [
      {
        path: 'db/:name',
        component: DatabaseDetailsComponent,
        pathMatch: 'full',
      },
      {
        path: 'db/:name/table/:table',
        component: TableDetailsComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainRoutingModule {}
